const getLocalName = (j, declarations, importName) => {
  if (importName === "default") {
    return declarations.find(j.ImportDefaultSpecifier).nodes()[0].local.name;
  } else {
    let specifier = declarations.find(
      j.ImportSpecifier,
      ({ imported }) => imported.name === importName
    );

    if (specifier.size() !== 1) return;

    return specifier.nodes()[0].local.name;
  }
};

const replaceOrAddJSXAttribute = (
  root,
  j,
  importLocation,
  importName,
  attributeName,
  getNewValue
) => {
  let hasTransformed = false;
  // find a matching declaration
  const declarations = root.find(
    j.ImportDeclaration,
    ({ source }) => source.type === "Literal" && source.value === importLocation
  );

  if (declarations.size() === 0) return hasTransformed;

  let localName = getLocalName(j, declarations, importName);
  if (!localName) return hasTransformed;

  let usages = root.findJSXElements(localName);

  usages.forEach(usage => {
    let attributes = usage.get("openingElement").get("attributes");
    let existingUsage = attributes.filter(({ node }) => {
      return (
        node.name.type === "JSXIdentifier" && node.name.name === attributeName
      );
    });
    hasTransformed = true;
    if (existingUsage.length < 1) {
      let newAttribute = j.jsxAttribute(
        j.jsxIdentifier(attributeName),
        j.literal(getNewValue())
      );
      attributes.push(newAttribute);
    } else {
      let value = existingUsage[0].get("value");
      let currentValue = value.node.value;
      let newValue = getNewValue(currentValue);
      value.node.value = newValue;
    }
  });
  return hasTransformed;
};

module.exports = replaceOrAddJSXAttribute;
