const replaceImportLocation = (root, j, oldLocation, newLocation) => {
  let regexpString = oldLocation.match(/\*$/)
    ? oldLocation
    : `${oldLocation}${"$"}`;

  let matcher = new RegExp(regexpString);

  // find a matching declaration
  const declarations = root.find(
    j.ImportDeclaration,
    ({ source }) => source.type === "Literal" && matcher.test(source.value)
  );

  if (declarations.size() === 0) return false;
  declarations.forEach(declaration => {
    let currentLocation = declaration.node.source.value;
    declaration.node.source.value = newLocation(currentLocation);
  });
  return true;
};

module.exports = replaceImportLocation;
