# wrap-conditional-render codemod

This codemod wraps a conditional render statement with another component.

## Usage

The first step is to identify conditionally rendered components that need to be wrapped. This might take some amount of investigation. If you have abstractions on-top of Atlaskit's `ModalDialog` or `Spotlight` which are conditionally rendered, you will need to identify these. For each component that needs to be transformed, add an entry in `codemode.config.js`. This could look like:

```js
const components = [
  // a named "Spotlight" import from "@atlaskit/onboarding"
  component(
    namedImport(n => n === "Spotlight"),
    importedFrom(p => p === "@atlaskit/onboarding")
  ),
  // any default import from "common/components/modal-dialog"
  component(
    defaultImport(),
    importedFrom(p => p === "common/components/modal-dialog")
  ),
  // any default import called "ChangeBoardingComponent"
  component(defaultImport(n => n === "ChangeBoardingComponent"), importedFrom())
];
```

The codemod will attempt to work out if the components described that list are conditionally rendered. If this is the case, a transition component will be wrapped around the conditional statement.

Depending on how many components you are transforming, you might need to update the `transitionComponentName` function. This function communicates which named import to add for a package name.

```js
const transitionComponentName = packageName =>
  packageName.toLowerCase().indexOf("modal") > -1
    ? "ModalTransition"
    : "SpotlightTransition";
```

Double check the other options exported from `codemod.config.js`. Ensure these options suit the tools your codebase uses.

## Running

In the directory of repo you want to run the codemod on:

```
jscodeshift -t ../atlaskit-codemods/src/wrap-conditional-render/codemod.js src --ignore-config .eslintignore
```

## Examples

These are examples of what types of expressions are transformed by this codemod.

Jsx boolean expressions.

```diff
- import Modal from '@atlaskit/modal-dialog';
+ import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

const App = ({ modalOpen }) => (
  <div>
+   <ModalTransition>
      {modalOpen && <Modal title="Hi there" />}
+   </ModalTransition>
  </div>
);
```

Jsx conditional expressions.

```diff
- import Modal from '@atlaskit/modal-dialog';
+ import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

const App = ({ modalOpen }) => (
  <div>
+   <ModalTransition>
      {modalOpen ? <Modal title="Hi there" /> : null}
+   </ModalTransition>
  </div>
);
```

Boolean expressions.

```diff
- import Modal from '@atlaskit/modal-dialog';
+ import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

class App extends React.Component {
  render() {
-   return this.props.modalOpen && <Modal title="Hi there" />;
+   return (
+    <ModalTransition>
+      {this.props.modalOpen && <Modal title="Hi there" />}
+    </ModalTransition>
+   );
  }
}
```

Conditional expressions.

```diff
- import Modal from '@atlaskit/modal-dialog';
+ import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

class App extends React.Component {
  render() {
-   return this.props.modalOpen ? <Modal title="Hi there" /> : null;
+   return (
+    <ModalTransition>
+      {this.props.modalOpen ? <Modal title="Hi there" /> : null}
+    </ModalTransition>
+   );
  }
}
```
