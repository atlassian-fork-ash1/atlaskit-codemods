// @flow

import React, { Component, type ComponentType } from "react";
import noop from "lodash/noop";
import ModalDialog from "common/components/modal-dialog";

type Props = {|
  isOpen: boolean,
  isSubmitAllowed: boolean,
  Content: ComponentType<{||}>,
  onClose: () => void,
  onSubmit: () => void,
  onCancel: () => void
|};

export default class Dialog extends Component<Props> {
  static defaultProps = {
    isOpen: false,
    isSubmitAllowed: false,
    onSubmit: noop,
    onCancel: noop,
    onClose: noop
  };

  render() {
    const {
      Content,
      isOpen,
      onClose,
      onSubmit,
      onCancel,
      isSubmitAllowed
    } = this.props;

    return isOpen ? (
      <ModalDialog
        heading="Frontend Selector"
        width={450}
        onClose={onClose}
        autoFocus
        actions={[
          {
            appearance: "primary",
            isDisabled: !isSubmitAllowed,
            onClick: onSubmit,
            text: "Submit"
          },
          {
            appearance: "link",
            onClick: onCancel,
            text: "Cancel"
          }
        ]}
      >
        <Content />
      </ModalDialog>
    ) : null;
  }
}
