const prettier = require("../prettier/prettier");
const {
  components,
  parser,
  prettier: shouldPrettier,
  transitionComponentName
} = require("./codemod.config.js");

const reduce = function(stepFn, initial) {
  let result = initial;
  this.forEach(function(path) {
    result = stepFn(result, path);
  });
  return result;
};

const containsComponents = components => importDeclaration =>
  components.reduce(
    (result, { both }) => result || both(importDeclaration),
    false
  );

const containsSpecifer = (specifiers, name) =>
  specifiers.filter(specifier => specifier.local.name === name).length > 0;

const toParent = fn => node => {
  if (node.parentPath === null) {
    return undefined;
  }
  if (fn(node.parentPath)) {
    return node.parentPath;
  }
  return toParent(fn)(node.parentPath);
};

const whereParent = fn => node => !!toParent(fn)(node);

const plugin = jscodeshift =>
  jscodeshift.registerMethods({
    reduce
  });

const transform = (j, root, components) => {
  let mutations = false;
  const importExpressions = root.find(
    j.ImportDeclaration,
    containsComponents(components)
  );
  if (importExpressions.length === 0) {
    return false;
  }
  // names of imports we are transforming as strings
  const componentNames = importExpressions.reduce(
    (names, importDeclaration) => [
      ...names,
      ...components
        .filter(({ both }) => both(importDeclaration.value))
        .map(({ specifier }) => specifier(importDeclaration.value))
    ],
    []
  );

  // all jsx elements that match the imports
  const jsxElements = root.find(
    j.JSXElement,
    jsx => !!componentNames.find(name => name === jsx.openingElement.name.name)
  );

  // jsx elements that are conditionally rendered
  const conditionalJsxElements = jsxElements.filter(
    whereParent(
      node =>
        node.value.type === "LogicalExpression" ||
        node.value.type === "ConditionalExpression"
    )
  );

  // imports for jsx elements that are conditionally rendered
  const conditionallyRenderedImports = importExpressions.filter(
    ({ value }) =>
      conditionalJsxElements.filter(jsx =>
        containsSpecifer(value.specifiers, jsx.value.openingElement.name.name)
      ).length > 0
  );

  // conditional render statements that get replaced
  const conditionalExpressions = conditionalJsxElements
    .map(
      toParent(
        node =>
          node.value.type === "LogicalExpression" ||
          node.value.type === "ConditionalExpression"
      )
    )
    .map(
      expression =>
        expression.parentPath.value.type === "JSXExpressionContainer"
          ? expression.parentPath
          : expression
    )
    // this step is a best effort to avoid transforming already wrapped components
    .filter(expression => {
      const parentCalledTransition = toParent(
        node =>
          node.value.type === "JSXElement" &&
          node.value.openingElement.name.name.indexOf("Transition") > -1
      )(expression);
      return !parentCalledTransition;
    });

  if (conditionalExpressions.length === 0) {
    return false;
  }

  // mutations

  conditionalExpressions.replaceWith((replace, i) => {
    mutations = true;
    // the jsxElement we are transforming e.g. <ModalDialog />
    const jsxElement = jsxElements.at(i).get();
    // the corresponding import name e.g. import ModalDialog from '@atlaskit/modal-dialog'
    const importExpression = conditionallyRenderedImports
      .filter(({ value }) =>
        containsSpecifer(
          value.specifiers,
          jsxElement.value.openingElement.name.name
        )
      )
      .get();
    const transitionElement = j.jsxIdentifier(
      transitionComponentName(importExpression.value.source.value)
    );
    // unwrap the expression if it is a jsx expression container
    const expression = replace.value.expression || replace.value;
    const newElement = j.jsxElement(
      j.jsxOpeningElement(transitionElement),
      j.jsxClosingElement(transitionElement),
      [j.jsxExpressionContainer(expression)]
    );
    return newElement;
  });

  conditionallyRenderedImports.replaceWith((importExpression, i) => {
    mutations = true;
    return {
      ...importExpression.value,
      specifiers: [
        ...importExpression.value.specifiers,
        j.importSpecifier(
          j.identifier(
            transitionComponentName(importExpression.value.source.value)
          )
        )
      ]
    };
  });

  return mutations;
};

module.exports = (file, api, options) => {
  const j = api.jscodeshift;
  j.use(plugin);
  const root = j(file.source);
  const mutations = transform(j, root, components);
  return mutations
    ? shouldPrettier
      ? prettier(root.toSource())
      : root.toSource()
    : null;
};

module.exports.parser = parser;
