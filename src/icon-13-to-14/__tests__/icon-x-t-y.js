"use strict";

const defineTest = require("jscodeshift/dist/testUtils").defineTest;

defineTest(__dirname, "codemod", null, "logo-transform");
defineTest(__dirname, "codemod", null, "file-type-transform");
defineTest(__dirname, "codemod", null, "jiraPriorityIconTransform");
