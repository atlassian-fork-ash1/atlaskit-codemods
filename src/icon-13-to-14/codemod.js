/*
Things we are handling:

1. product icon moved to logo:
    a) find imports of product icons and replace path
    b) find instances where they are being rendered and find the size they are being rendered at
    c) translate the property to the related one:
        xsmall -> small
        medium -> small
        large -> medium
        xlarge -> xlarge
    d) provide warning/info about where things were changed
    e) Provide a recommended version of Logo and Icon which should be in the pkgJson

2. object/file-type icons removed
    a) find uses of object and file type icons and replace path with path to relevant size
    b) check if components are using the 'size' prop, and if they are provide LOUD WARNINGS that these will no longer work
    c) Provide a recommended version of Icon packages which should be in the pkgJson

3. jiraMajorIcon and jiraMinorIcon are pointing in the wrong directions
    a) find instances where these icons are imported
    b) LOUD WARNINGS that these have changed, do not edit people's code
*/
const prettier = require("../prettier/prettier");
const replaceOrAddJSXAttribute = require("../utils/replaceOrAddJSXAttribute");
const replaceImportLocation = require("../utils/replaceImportLocation");

const logoMap = {
  "@atlaskit/icon/glyph/atlassian":
    "@atlaskit/logo/dist/esm/AtlassianLogo/Icon",
  "@atlaskit/icon/glyph/bitbucket":
    "@atlaskit/logo/dist/esm/BitbucketLogo/Icon",
  "@atlaskit/icon/glyph/confluence":
    "@atlaskit/logo/dist/esm/ConfluenceLogo/Icon",
  "@atlaskit/icon/glyph/hipchat": "@atlaskit/logo/dist/esm/HipchatLogo/Icon",
  "@atlaskit/icon/glyph/jira-core": "@atlaskit/logo/dist/esm/JiraCoreLogo/Icon",
  "@atlaskit/icon/glyph/jira": "@atlaskit/logo/dist/esm/JiraLogo/Icon",
  "@atlaskit/icon/glyph/jira-service-desk":
    "@atlaskit/logo/dist/esm/JiraServiceDeskLogo/Icon",
  "@atlaskit/icon/glyph/jira-software":
    "@atlaskit/logo/dist/esm/JiraSoftwareLogo/Icon",
  "@atlaskit/icon/glyph/statuspage":
    "@atlaskit/logo/dist/esm/StatuspageLogo/Icon",
  "@atlaskit/icon/glyph/stride": "@atlaskit/logo/dist/esm/StrideLogo/Icon"
};

const oldFileTypePath = "@atlaskit/icon/glyph/file-types/*";
const oldObjectPath = "@atlaskit/icon/glyph/objects/*";

const logoSizeMap = {
  default: "small",
  small: "xsmall",
  medium: "small",
  large: "medium",
  xlarge: "xlarge"
};

const getNewSizeValues = (oldValue = "default") => logoSizeMap[oldValue];

const getFileTypePathFromOld = oldPath => {
  let sadMatchingRegex = new RegExp(`/file-(\\d\\d)-(.*)`);

  let [_, size, fileName] = oldPath.match(sadMatchingRegex);

  return `@atlaskit/icon-file-type/glyph/${fileName}/${size}`;
};

const getObjectPathFromOld = oldPath => {
  let sadMatchingRegex = new RegExp(`/file-(\\d\\d)-(.*)`);

  let [_, size, fileName] = oldPath.match(sadMatchingRegex);

  return `@atlaskit/icon-object/glyph/${fileName}/${size}`;
};

const swapImports = (root, j) => {
  const declarations = root.find(
    j.ImportDeclaration,
    ({ source }) =>
      source.type === "Literal" &&
      (source.value === "@atlaskit/icon/glyph/jira/major" ||
        source.value === "@atlaskit/icon/glyph/jira/minor")
  );
  declarations.forEach(declaration => {
    declaration.node.source.value =
      declaration.node.source.value === "@atlaskit/icon/glyph/jira/major"
        ? "@atlaskit/icon/glyph/jira/minor"
        : "@atlaskit/icon/glyph/jira/major";
  });
  return true;
};

const transform = (j, root) => {
  let messages = [];
  let importsToReplace = Object.entries(logoMap);

  importsToReplace.forEach(([oldImport, newImport]) => {
    let firstTransform = replaceOrAddJSXAttribute(
      root,
      j,
      oldImport,
      "default",
      "size",
      getNewSizeValues
    );
    if (firstTransform) {
      messages.push(
        `The size prop has been changed from ${oldImport} to ${newImport}, and has been updated`
      );
    }
    let secondTransform = replaceImportLocation(
      root,
      j,
      oldImport,
      () => newImport
    );
    if (secondTransform)
      messages.push(
        `The import location has changed from ${oldImport} to ${newImport}, and has been updated`
      );
  });

  let thirdTransform = replaceImportLocation(
    root,
    j,
    oldFileTypePath,
    getFileTypePathFromOld
  );
  let fourthTransform = replaceImportLocation(
    root,
    j,
    oldObjectPath,
    getObjectPathFromOld
  );
  let fifthTransform = swapImports(root, j);
  if (thirdTransform)
    messages.push(
      `An icon you were using has been moved into the new @atlaskit/icon-file-type package. You will need to add this package to your package.json.`
    );
  if (fourthTransform)
    messages.push(
      `An icon you were using has been moved into the new @atlaskit/icon-object package. You will need to add this package to your package.json.`
    );
  if (fifthTransform)
    messages.push(
      `We have swapped a jiraPriorityIcon, as these were previously misnamed. You may want to update your local names`
    );
  return messages;
};

module.exports = (file, { jscodeshift: j }, options) => {
  const root = j(file.source);
  let messages = transform(j, root);

  if (messages.length > 0) {
    console.log(
      `====================================
transformation occured in ${file.path}\n${messages.join("\n")}
====================================`
    );
    return prettier(root.toSource());
  } else {
    return file.source;
  }
};

module.exports.parser = "flow";
