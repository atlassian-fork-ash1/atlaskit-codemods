import somethingElse from "./somewhere";
import { somethingMoreElse } from "./somewhereElse";
import AtlassianIcon from "@atlaskit/icon/glyph/atlassian";

export default (
  <div>
    <AtlassianIcon />
    <AtlassianIcon size="small" secondProp="a string" />
    <AtlassianIcon size="medium" />
    <AtlassianIcon size="large" />
    <AtlassianIcon size="xlarge" />
  </div>
);
