import JiraMajorIcon from "@atlaskit/icon/glyph/jira/major";
import JiraMinorIcon from "@atlaskit/icon/glyph/jira/minor";

export default () => (
  <Fragment>
    <JiraMajorIcon />
    <JiraMinorIcon />
  </Fragment>
);
