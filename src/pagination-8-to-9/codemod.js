const prettier = require("../prettier/prettier");
const replaceOrAddJSXAttribute = require("../utils/replaceOrAddJSXAttribute");
const {
  parser,
  prettier: shouldPrettier,
  transitionComponentName
} = require("./codemod.config.js");

function totalPropTransform(usage, j) {
  let value = usage.get("value");
  let name = usage.get("name");
  let currentValue = value.node.expression;
  name.node.name = "pages";
  if (currentValue.type === "Literal") {
    const t = j.arrayExpression(
      [...Array(currentValue.value)].map((_, i) => j.literal(i + 1))
    );
    value.node.expression = t;
  } else if (currentValue.type === "Identifier") {
    const toArraySpread = size =>
      `[...Array(${size})].map((_, index) => index + 1)`;
    value.node.expression = toArraySpread(value.node.expression.name);
  } else {
    console.log(
      `This codemod does not support the use case in the file: ${file.path}`
    );
  }
}

function onChangePropTransform(attribute, j) {
  if (attribute.node.value.expression.type === "ArrowFunctionExpression") {
    const params = attribute.node.value.expression.params;
    const newParams = params[0].name;
    attribute.node.value.expression.params = ["_", newParams];
  } else if (
    attribute.node.value.expression.type === "MemberExpression" ||
    attribute.node.value.expression.type === "Identifier"
  ) {
    attribute.node.value.expression = j.arrowFunctionExpression(
      [j.identifier("_"), j.identifier("selectedPageIndex")],
      j.callExpression(attribute.node.value.expression, [
        j.identifier("selectedPageIndex")
      ])
    );
  } else {
    console.log(
      `This codemod does not support the use case in the file: ${file.path}`
    );
  }
}

function defaultValuePropTransform(attribute, j) {
  let name = attribute.get("name");
  name.node.name = "defaultSelectedIndex";
}

function valuePropTransform(attribute, j) {
  let name = attribute.get("name");
  name.node.name = "selectedIndex";
}

function transform(j, root, file) {
  let hasTransformed = false;
  // Find usage
  const declarations = root.find(
    j.ImportDeclaration,
    ({ source }) =>
      source.type === "Literal" && source.value === "@atlaskit/pagination"
  );

  if (declarations.size() === 0) return false;

  let localName = declarations.find(j.ImportDefaultSpecifier).nodes()[0].local
    .name;
  if (!localName) return false;

  let usages = root.findJSXElements(localName);

  usages.forEach(usage => {
    let attributes = usage.get("openingElement").get("attributes");
    attributes.each(attribute => {
      if (attribute.node.name.type !== "JSXIdentifier") return;
      if (attribute.node.name.name === "total") {
        totalPropTransform(attribute, j);
        hasTransformed = true;
      }
      if (attribute.node.name.name === "onChange") {
        onChangePropTransform(attribute, j);
        hasTransformed = true;
      }
      if (attribute.node.name.name === "defaultValue") {
        defaultValuePropTransform(attribute, j);
        hasTransformed = true;
      }
      if (attribute.node.name.name === "value") {
        valuePropTransform(attribute, j);
        hasTransformed = true;
      }
    });
  });
  return hasTransformed;
}

module.exports = (file, { jscodeshift: j }) => {
  const root = j(file.source);
  const mutations = transform(j, root, file);

  return mutations
    ? shouldPrettier
      ? prettier(root.toSource())
      : root.toSource()
    : null;
};

module.exports.parser = parser;
