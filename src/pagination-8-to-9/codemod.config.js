const options = {
  // parser for jscodeshift to use
  parser: "flow",
  // run prettier on output, will respect local prettier config
  prettier: true
};

module.exports = options;
