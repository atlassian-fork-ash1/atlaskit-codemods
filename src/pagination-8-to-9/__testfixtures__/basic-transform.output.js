// @flow
import Pagination from "@atlaskit/pagination";

export default () => (
  <Pagination
    defaultSelectedIndex={1}
    selectedIndex={4}
    pages={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
    onChange={(_, e) => console.log("page changed", e)}
  />
);
