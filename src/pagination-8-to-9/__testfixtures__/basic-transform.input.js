// @flow
import Pagination from "@atlaskit/pagination";

export default () => (
  <Pagination
    defaultValue={1}
    value={4}
    total={10}
    onChange={e => console.log("page changed", e)}
  />
);
