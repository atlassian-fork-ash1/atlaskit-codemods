# pagination-8-to-9 codemod

This codemod will upgrade the pagination code in your repository form version 8 to version 9

## Usage

## Running

In the directory of repo you want to run the codemod on:

```
jscodeshift -t ../atlaskit-codemods/src/pagination-8-to-9/codemod.js src --ignore-config .eslintignore
```

## Examples

These are examples of what types of expressions are transformed by this codemod.

Pagination usage:

```diff
    <Pagination
-       value={currentPage}
-       total={totalPages}
-       onChange={this.onPageChange}
+       selectedIndex={currentPage}
+       pages={[...Array(totalPages)].map((_, index) => index + 1)}
+       onChange={(_, selectedPageIndex) => this.onPageChange(selectedPageIndex)}
       i18n={{
            prev: formatMessage(messages.previousButton),
            next: formatMessage(messages.nextButton),
        }}
    />
);
```
